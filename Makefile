# Исходные файлы
LIB_SRC=lib.asm
DICT_SRC=dict.asm
MAIN_SRC=main.asm

# Объектные файлы
LIB_OBJ=$(LIB_SRC:.asm=.o)
DICT_OBJ=$(DICT_SRC:.asm=.o)
MAIN_OBJ=$(MAIN_SRC:.asm=.o)

# Итоговый исполняемый файл
EXECUTABLE=my_dic

# Настройки компиляции и линковки
AS=nasm
ASFLAGS=-f elf64
LD=ld
LDFLAGS=

all: $(EXECUTABLE)

$(EXECUTABLE): $(LIB_OBJ) $(DICT_OBJ) $(MAIN_OBJ)
	$(LD) $(LDFLAGS) -o $@ $^

%.o: %.asm
	$(AS) $(ASFLAGS) $< -o $@

test: all
	python3 test_script.py

clean:
	rm -f $(LIB_OBJ) $(DICT_OBJ) $(MAIN_OBJ) $(EXECUTABLE)
