%include "lib.inc"
global find_word

section .text

find_word:
    ; Вход:
    ; rdi - указатель на нуль-терминированную строку
    ; rsi - указатель на начало словаря
    ; Выход:
    ; rax - адрес начала вхождения в словарь (не значения), если найдено, иначе 0
    
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi


.loop:
    test r13, r13
    jz .not_found

    mov rsi, r12
    lea rdi, [r13 + QUAD_WORD]

    call string_equals

    cmp rax, 1
    je .found

    mov r13, [r13]

    jmp .loop

.not_found:
    pop r13
    pop r12
    xor rax, rax
    ret

.found:
    mov rax, r13
    pop r13
    pop r12
    ret
