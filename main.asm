%include "lib.inc"
%include "dict.inc"
%include "words.inc"


section .bss
buffer resb 256

section .rodata
    error_message db "Word not found in dictionary.", 0
    read_error_message db "Error reading word from stdin.", 0

section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, 255  ; ограничение на длину

    call read_word
    test rax, rax
    jz .failed_to_read
    
    ; Пытаемся найти вхождение в словаре
    mov rdi, rax
    mov rsi, first_word
    call find_word

    test rax, rax
    jz .not_found
    
    ; Выводим значение по ключу в stdout
    lea rdi, [rax + QUAD_WORD]
    call string_length
    lea rdi, [rdi + rax + 1]
    call print_string
    call print_newline
    call exit

.not_found:
    mov rdi, error_message
.exit_error:
    call errprint_string
    call print_newline
    call exit

.failed_to_read:
    mov rdi, read_error_message
    jmp .exit_error
