import subprocess

input_file = "input.txt"
expected_output_file = "expected_output.txt"
expected_error_file = "expected_error.txt"  # Новый файл с ожидаемыми ошибками
executable = "./my_dic"

with open(input_file, 'r') as inputs, open(expected_output_file, 'r') as expected_outputs, open(expected_error_file, 'r') as expected_errors:
    for input_line, expected_output_line, expected_error_line in zip(inputs, expected_outputs, expected_errors):
        input_line = input_line.strip()
        expected_output_line = expected_output_line.strip()
        expected_error_line = expected_error_line.strip()

        result = subprocess.run([executable], input=input_line, text=True, capture_output=True)

        if result.stdout.strip() != expected_output_line or result.stderr.strip() != expected_error_line:
            print(f"Test failed!\nInput: '{input_line}'\nExpected stdout: '{expected_output_line}'\nGot stdout: '{result.stdout.strip()}'\nExpected stderr: '{expected_error_line}'\nGot stderr: '{result.stderr.strip()}'")
            break
    else:
        print("All tests passed!")
